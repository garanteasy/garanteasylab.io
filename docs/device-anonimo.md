---
title: Device anonimo
---
​13.2​ Device anonimo
---------------------

Quando il licenziatario non deve disporre della possibilità di ricercare e visualizzare le prove di acquisto memorizzate dagli acquirenti come per esempio avviene quando gli acquirenti registrano autonomamente i propri documenti di acquisto (dispositivi dislocati in luoghi pubblici uffici ecc) è possibile seguire il flusso che segue.

Questa procedura è idonea per esempio al caso di fotocopiatrici smart, totem, internet point, ...

L’utente che esegue la login deve avere il privilegio `ADD_RECEIPT_FILE`.

1.  Autenticazione `POST:/authenticate` con le proprie credenziali per reperire il token di autorizzazione da aggiungere all’header HTTP Authorization di tutte le chiamate successive.
2.  Memorizzazione del documento di acquisto con parametri:

1.  Sorgente della informazione impostata a DATAENTY in modo da consentire all’acquirente future modifiche - "source": "DATAENTRY"
2.  Attributo customer impostato con la mail dell’acquirente in modo che venga registrato se non non lo è già - `"customer": { "user": { "login": "email dell’acquirente" }}}`
