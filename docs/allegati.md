---
title: Allegati alla Receipt
tags:
- pdf
- jpg
- image
- immagine
- scontrino
---
​5​ Trasferimento di documenti allegati
=======================================

I file da allegare al documento di acquisto (copia di scontrino, fattura o quant’altro possa essere utile all’esercizio della garanzia) possono essere trasferiti dopo la memorizzazione del documento medesimo con la chiamata

    POST http://localhost/api/receipt-files

il cui payload indica l’identificativo del documento ottenuto dalla POST/receipts nell’attributo receiptId.

L’attributo fileData contiene il file ca trasferire encodato base64.

L’attributo "`fileContentType`" indica il mime type del file (p.e. application/pdf, image/jpeg, ...).

Il "`fileName`" riporta il nome del file come utilizzato sul dispositivo client.

Per esempio:
```json
{
    "receiptId":53241,
    "fileData": "...",
    "fileContentType": "...",
    "fileName": "..."
}
```
