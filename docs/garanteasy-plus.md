---
title: Garanteasy Basic
---
# Garanteasy PLUS

Il servizio "PLUS" prevede la visualizzazione della sola copia digitale dello scontrino indipendentemente dalla presenza o meno dei dettagli dello scontrino, l'eventuale presenza di prodotti acquistati con le relative schede di dettaglio e le garanzie associate.

Il servizio PLUS viene erogato dalla piattaforma:

* Se l'acquirente ha inviato una copia digitale dello scontrino come allegato ad una mail da lui indirizzata a archivio@garanteasy.com (o archive@garanteasy.com).
* Se l'acquirente ha accettato presso la cassa di un retailer convenzionato il servizio BASIC

Qualora l'acquirente accetti il servizio in cassa, il cassiere dovrà battere sullo scontrino il prodotto con codice `GARANTEASY-PLUS`; successivamente lo scontrino completo di tutti i dati verrà inviato a Garanteasy tramite la coppia di [API di memorizzazione del documento di acquisto](memorizzazione-receipt.md) e [associazione di un allegato](allegati.md).

In un secondo momento l'acquirente potrebbe:

* acquistare un abbonamento annuale nel qual caso la visualizzazione dei dettagli viene "liberata"
* presentarsi in negozio per richiedere la correzione dell'indirizzo mail associato allo scontrino