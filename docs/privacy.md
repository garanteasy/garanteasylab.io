---
title: Gestione della privacy
---
​12​ Gestione della privacy
===========================

La gestione della privacy dei dati applicata da Garanteasy rispecchia i migliori standard di protezione dati. La gestione è effettuata in completa conformità con il nuovo regolamento EU 679/2016 - GDPR e della sua Norma di recepimento, D.lgs 101/2018.

Per ogni informazione sul tema Privacy, consultare il sito www.garanteasy.com .

Quando i dati vengono comunicati da un merchant partner questi ha la possibilità di accedere ai dati caricati tramite le apposite interfacce di servizio, pertanto l’utente deve preventivamente dargli il consenso all’uso dei dati nominandolo così titolare; in questo contesto Garanteasy assume il ruolo di responsabile del trattamento dei dati.
