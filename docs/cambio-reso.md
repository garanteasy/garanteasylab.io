---
title: Sostituzione o reso di un prodotto
---
​9​ Gestione cambio e reso prodotto
===================================

La sostituzione o restituzione di un prodotto può avvenire in più modi; qualora si renda necessario modificare il documento di acquisto per indicare la restituzione di un prodotto sarà necessario invocare l’entry point ‘receipt/return-product’.

La chiamata

    PUT /api/receipts/withdrawal

accetta nel body una struttura di Receipt (come per la chiamata di memorizzazione) valorizzata con i soli estremi necessari e sufficienti ad identificare univocamente un documento e gli estremi dei prodotti restituiti:

*   negozio
*   data acquisto
*   numero ricevuta
*   numero cassa
*   elenco prodotti restituiti

Gli attributi di ciascun prodotto sono i medesimi comunicati in fase di registrazione del documento di acquisto.

per esempio
```json
{
    "receiptNumber": "27(6)",
    "buyingDate": "2020-09-16",
    "counterCode": "SMW101",
    "shop": {
        "code": "SHOP1",
        "merchant": {
            "code": "PARTNERCODE"
        }
    },
    "purchasedProducts": [{
            "product": {
                "sku": "JBL05PARTYBX100",
                "ean": "6925281956676",
                "name": "JBL-PARTYBOX100  MAXI HIFI BT",
                "brand": {
                    "name": "JBL",
                    "code": "JBL"
                }
            },
            "quantity": 1
        }
    ]
}
```

La chiamata torna `200 OK` (l’identificativo della ricevuta modificata si trova nel body della risposta) oppure `404 NOT FOUND` qualora gli estremi comunicati non fossero sufficienti ad individuare un documento di acquisto; l’assenza di uno o più degli estremi del documento provoca un errore `400 BAD REQUEST` il cui body contiene una indicazione relativa all’attributo errato o mancante.

In caso di successo, i prodotti identificati assumono lo stato `RETURNED` e le eventuali garanzie (legali, convenzionali, ...) vengono stralciate. L’eventuale successivo scontrino relativo all’articolo che sostituisce quello restituito può essere comunicato con codice [`GARANTEASY-PREPAGATO`](prepagato).
