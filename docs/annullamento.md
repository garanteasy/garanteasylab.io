---
title: Annullamento d un documento di acquisto
---
​8​ Annullamento documento di acquisto
======================================

L’annullamento di un documento di acquisto avviene chiamando l’entry point

    PUT /api/receipts/withdrawal

questo accetta nel body una struttura di Receipt (come per la chiamata di memorizzazione) valorizzata solo con i soli estremi necessari e sufficienti ad identificare univocamente un documento:

*   negozio
*   data acquisto
*   numero ricevuta
*   numero cassa

per esempio
```json
{
    "receiptNumber": "27(6)",
    "buyingDate": "2020-09-16",
    "counterCode": "SMW101",
    "shop": {
        "code": "SHOP1",
        "merchant": {
            "code": "PARTNERCODE"
        }
    }
}
```
La chiamata torna `200 OK` (l’identificativo della ricevuta modificata si trova nel body della risposta) oppure `404 NOT FOUND` qualora gli estremi comunicati non fossero sufficienti ad individuare un documento di acquisto; l’assenza di uno o più degli estremi del documento provoca un errore `400 BAD REQUEST` il cui body contiene una indicazione relativa all’attributo errato o mancante.

In caso di successo lo scontrino va in stato `WITHDRAWED` e viene conteggiato a parte nei report dell'amministrazione in modo che possa essere scorporato dai conteggi.
