---
title: Lettura di un documento di acquisto
---
​6​ Lettura di un documento di acquisto
=======================================

E’ possibile accedere a tutti i dettagli di un documento di acquisto registrato utilizzando il suo identificativo

    GET /receipt/{id}

La chiamata restituisce `200 OK` con i dati del documento di acquisto oppure `404 NOT FOUND` qualora l’identificativo non venisse trovato.
