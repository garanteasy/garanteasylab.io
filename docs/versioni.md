---
title: History
---
Versioni

27-10-2020 v.1.1 Aggiunti dettagli prodotto acquistato

01-12-2020 v.1.2 Annullamento documento di acquisto

03-02-2021 v.1.3 Gestione prodotti resi

26-03-2021 v.1.4 Documentazione ambiente di test

06-04-2021 v.1.5 Obbligatorietà indirizzo per gli ecommerce

14-06-2021 v.1.6 Documentati i [codici prodotto standard](codici-prodotto-garanteasy)

29-06-2021 v.1.7 Aggiornata la struttura della Receipt

06-10-2021 v.1.8 Aggiunti i codici prodotto bundle

03-01-2022 v.19 Introdotto lo stato 409 per le chiamate in conflitto (p.e. scontrini duplicati)