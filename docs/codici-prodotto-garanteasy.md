---
title: Codici prodotto Garanteasy
---
Codici prodotto Garanteasy
==========================
I documenti di acquisto devono contenere il prodotto corrispondente al servizio garanteasy venduto; il "*codice EAN*" utilizzabile è uno fra questi:

* [GARANTEASY-PLUS](garanteasy-plus.md) - Consente all'acquirente la visualizzazione della sola copia digitale del documento di acquisto
* GARANTEASY - E' il servizio che consente la memorizzazione di un singolo documento di acquisto comprensivo di schede prodotto, garanzie legali, garanzie del produttore ed eventuali estensioni di garanzia
* GARANTEASY_AAAA - (p.e. GARANTEASY_2021) Abbonamento per l'anno AAAA, la **prima vendita**, quella che corrisponde alla sottoscrizione dell'abbonamento deve avere **prezzo valorizzato**, *li **utilizzi successivi** (fino al 31 dicembre dell'anno di abbonamento) devono indicare **prezzo zero** €
* GARANTEASY-GIFT - Ci consente di distinguere i prodotti destinati ad essere regalati, il destinatario del regalo (identificato dalla mail) riceve una mail apposita
* GARANTEASY-DIPENDENTE - Prevede la memorizzazione di un documento di acquisto al prezzo convenzionato
* GARANTEASY-PREPAGATO - Viene utilizzato per esempio nel contesto della [gestione delle sostituzioni](cambio-reso)
* GARANTEASY-BUNDLE-ESTENSIONE - Viene utilizzato quando il servizio Garanteasy è venduto in bundle
* GARANTEASY-BUNDLE-PRODOTTO - Viene utilizzato quando il servizio Garanteasy è venduto in bundle
* GARANTEASY-BUNDLE-SCONTRINO - Viene utilizzato quando il servizio Garanteasy è venduto in bundle

Se non fosse possibile utilizzare in cassa l'EAN indicato è necessario comunicare quale SKU verrà usato per ciascun prodotto in modo da potere configurare la corretta associazione con il prodotto sulla base del licenziatario.

