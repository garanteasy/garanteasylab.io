---
title: Applicazione di esempio
---
Applicazione di esempio
=======================

E' diponibile [su gitlab](https://gitlab.com/garanteasy/import-merchant) una applicazione di esempio che implementa la trasformazione di un file CSV per poi effettuare il trasferimento dei dati attraverso le API Rest.
