module.exports = {
    title: 'GitLab ❤️ Garanteasy',
    description: 'Garanteasy public technical documentation',
    base: '/',
    dest: 'public',
    themeConfig: {
        sidebar: ['/',
            'intro',
            'modalita',
            'auth',
            'memorizzazione-receipt',
            'receipt',
            'allegati',
            'get-receipt',
            'trasferimento',
            'annullamento',
            'cambio-reso',
            'prepagato',
            'error-codes',
            'privacy',
            'device-ecommerce',
            'device-anonimo',
            'codici-prodotto-garanteasy',
            'test-environment',
            'esempi',
            'versioni'
        ],
        displayAllHeaders: false,
        repo: 'https://gitlab.com/garanteasy/garanteasy',
        docsRepo: 'https://gitlab.com/garanteasy/garanteasy.gitlab.io',
        docsDir: 'docs',
        editLinks: true,
        lastUpdated: 'Last Updated', // string | boolean
    },
    plugins: [
        'mermaidjs'
    ]
}
