---
title: Autenticazione
---
2​. API di Autenticazione
=========================

Le API Garanteasy sono protette dallo schema di autenticazione JWT.

Le credenziali dell'utente che accede alla piattaforma non devono essere necessariamente memorizzate nella app, è sufficiente memorizzare il token di autorizzazione ottenuto in risposta alla prima autenticazione. Il token dovrà essere rinnovato al suo scadere fornendo nuovamente le credenziali dell'utente.

Il token di autorizzazione si può ottenere con la chiamata

    POST /authenticate

con payload username e password.

Per esempio:

    curl 'http://localhost:8081/api/authenticate' --data-binary '{"username":"luca.merchant@garanteasy.com","password":"Password1"}' -H 'Content-Type: application/json;charset=UTF-8'

Se le credenziali sono corrette la chiamata torna lo stato 200 (OK) e una struttura contenente il token di autorizzazione da utilizzare per le chiamate successive:

```json
{

 "id_token" : "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsdWNhLm1lcmNoYW50QGdhcmFudGVhc3kuY29tIiwiYXV0aCI6Ik1FUkNIQU5UX1BST1ZBQUFBQSxST0xFX01FUkNIQU5ULFJPTEVfVVNFUiIsImV4cCI6MTU1NDgzMTUxMH0.kseLQL8r0-4fNyRm0Eg\_qBAgqDhJbnFsqL2IqORyjweqfLgwbcKG5bqGy5NDQ67k1UoOAnao1fssWfcwk-NwAg"

}
```

Tutte le chiamate successive devono presentare l’header Authorizazion valorizzato con la dicitura Bearer seguita dal valore del token di autorizzazione.

Per esempio:

     curl 'http://localhost:8081/api/receipts' -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsdWNhLm1lcmNoYW50QGdhcmFudGVhc3kuY29tIiwiYXV0aCI6Ik1FUkNIQU5UX1BST1ZBQUFBQSxST0xFX01FUkNIQU5ULFJPTEVfVVNFUiIsImV4cCI6MTU1NDgzMTUxMH0.kseLQL8r0-4fNyRm0Eg\_qBAgqDhJbnFsqL2IqORyjweqfLgwbcKG5bqGy5NDQ67k1UoOAnao1fssWfcwk-NwAg' -H 'Content-Type: application/json;charset=UTF-8' ...

NB Per brevità negli esempi che seguono l’header di autorizzazione verrà omesso.

​