---
title: Ambiente di test
---
Ambiente di test
================

L’ambiente di test espone i seguenti entry point:

* https://webapp-test.garanteasy.com →         Istanza di Garanteasy con interfaccia utente e API
* https://partner-test.garanteasy.com →         Interfaccia per i partner
* https://db-test.garanteasy.com →         Istanza di adminer per l’eventuale osservazione dei dati
* https://cms-test.garanteasy.com →         Istanza CMS per la gestione di garanzie etc
* https://log-test.garanteasy.com →        Log applicativi
* https://mail-test.garanteasy.com →         Istanza di MailHog, in sostanza una "meta casella di posta" che consente di osservare tutte le mail inviate dal sistema (per sicurezza nessuna mail dell’ambiente di test raggiunge la reale casella di destinazione, vengono tutte "catturate" da MailHog).
