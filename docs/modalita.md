---
title: Modalità, formati e naming di trasmissione dati
---​
1​. Modalità, formati e naming di trasmissione dati
===================================================

Il Licenziatario ha a disposizione le due modalità di trasmissione:

1.  FTP (file transfer)

    1.  invio dal Merchant su macchina Garanteasy del file con i dati
    2.  Garanteasy che legge sulla macchina del Licenziatario il file con i dati

2.  API ovvero applicativo del Licenziatario che chiama API (REST/Json con autenticazione JWT-Json Web Tokens) della Piattaforma Garanteasy

In questo documento tratteremo il solo trasferimento tramite le API, adatto ai contesti dove la sincronicità della memorizzazione delle prove di acquisto è un requisito oppure laddove il volume del venduto dovesse essere abbastanza contenuto.

Il licenziatario comunica i dati anagrafici ogniqualvolta se ne renda necessario l’aggiornamento. I dati contrassegnati con (*) sono obbligatori.

Tutte le interfacce della piattaforma sono completamente implementate sfruttando le API [REST](https://it.wikipedia.org/wiki/Representational_State_Transfer) esposte dal suo core; il colloquio avviene attraverso lo scambio di dati in formato JSON. L'endpoint del servizio di produzione è https://webapp.garanteasy.com/api/.

A completamento di questo documento sono disponibili degli esempi di integrazione accessibili su https://gitlab.com/garanteasy/integrazioni/integration-samples
