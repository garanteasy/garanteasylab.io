---
title: Strutture dati
tags:

- receipt
- scontrino
- ricevuta
- fattura

---
​4.​ Struttura Receipt
=====================

​4.1​ Profilo acquisto - `profile`
--------------------------------

Determina la tipologia di garanzia legale attribuibile ai prodotti acquistati.

Può assumere i valori

* `0` acquisto effettuato a titolo personale (default)
* `1` acquisto professionale

​4.2​ Tipologia di documento - `receiptType`
------------------------------------------

Può assumere i valori:

* `RECEIPT` - Scontrino
* `OFFICIAL_RECEIPT` - Ricevuta fiscale
* `INVOICE` - Fattura

Non è obbligatorio, per default assume il valore `RECEIPT`.

​4.3​ Numero documento - `receiptNumber` (obbligatorio)
-----------------------------------------------------

Numero identificativo del documento di acquisto.

E’ utile ad identificare il documento qualora fosse necessario esercitare il diritto alla garanzia.

​4.4​ Numero cassa - `counterCode` (obbligatorio per gli scontrini)
-----------------------------------------------------------------

Per gli scontrini emessi da una cassa contiene il numero di cassa.

​4.5​ Descrizione - `description`
-------------------------------

Descrizione in testo libero del documento di acquisto, aiuta l’utente ad identificare il documento

​4.6​ Data creazione - `creationDate` (readonly)
-----------------------------------

Data di registrazione del documento di acquisto sulla piattaforma nel formato AAAA-MM-GG.

Per default assume il valore della data corrente.

​4.7​ Data Acquisto - `buyingDate` (obbligatorio)
-----------------------------------------------

Rappresenta la data in cui è stato effettuato l’acquisto in formato AAAA-MM-GG.

​4.8​ Tipo di pagamento - `paymentType`
-------------------------------------

Può assumere i valori:

* CASH
* CHEQUE
* BANK
* ATM_CARD
* PAYPAL
* VISA
* MASTERCARD
* AM_EXP
* DINERS
* OTHER
* DEBIT_CARD

​4.9​ Fonte dati - `source` (obbligatorio)
----------------------------------------

Identifica la fonte dell’informazione.

I soli valori rilevanti in questo contesto sono:

* `MERCHANT` quando i dati sono caricati da un merchant come ad esempio un ecommerce (l’acquirente non può più
  modificare le informazioni indicate dal merchant)
* `MAIL` quando i dati vengono inviati via mail (l’acquirente ha il pieno controllo su tutte le informazioni)
* `DATAENTRY` quando i dati vengono caricati direttamente dall’acquirente

​4.10​ Importo transato - `transactionAmount`
-------------------------------------------

Importo (numerico) totale del documento di acquisto. Utilizzare il punto "." come separatore dei decimali, non
utilizzare un separatore delle migliaia.

​4.11​ Valuta - `currency`
------------------------

Codice della valuta utilizzata nella transazione. Utilizza i valori
alfabetici [ISO 4217](https://it.wikipedia.org/wiki/ISO_4217#Codici_attivi) per identificare la valuta.

Per esempio:

```json
{
  ...
  "currency": "EUR"
}
```

​4.12​ Quantità di prodotti acquistati - `numProducts`
----------------------------------------------------

Quantità di prodotti acquistati.


4.13 Acquirente - `customer`
------------------------------

Descrive le caratteristiche dell’acquirente con gli attributi:

* `user` - struttura che descrive l’utente che potrà poi accedere alla ricevuta (obbligatorio) definita dal solo
  attributo login obbligatorio e valorizzato con la mail dell’acquirente
* `legalName` - nome e cognome o ragione sociale (obbligatorio)
* `vatId` - partita iva

Per esempio:

```json
{
  "customer": {
    "user": {
      "login": "[luca.orlandi@gmail.com](mailto:luca.orlandi@gmail.com)"
    },
    "legalName": "ACME Srl.",
    "vatId": "IT01234567891"
  }
}
```

la struttura dell'indirizzo è descritta nel paragrafo apposito.

​4.14​ Indirizzo dell’acquirente - `customerAddress` (obbligatorio per e-commerce)
--------------------------------------------------------------------------------

La definizione dell’indirizzo dell’acquirente è obbligatoria per i documenti di acquisto emessi dai siti e-commerce in
quanto la determinazione delle garanzie convenzionali prevede la valutazione della nazione di residenza del cliente.

Indirizzo dell’acquirente caratterizzato da:

* `street` - via
* `streetNumber` - numero civico
* `locality` - eventuale località
* `city` - nome della città
* `region` - codice della provincia o regione per gli stati che la utilizzano
* `postCode` - Codice di Avviamento Postale
* `country` - struttura che rappresenta il paese; il campo "code" utilizza i codici a due caratteri indicati nella
  convenzione [ISO 3166-1](https://it.wikipedia.org/wiki/ISO_3166-1#Lista_dei_codici_assegnati_ufficialmente) (
  obbligatorio)

Per esempio:

```json
{
  "street": "Via Pio XI",
  "streetNumber": "1",
  "city": "Milano",
  "region": "MI",
  "postCode": "20123",
  "country": {
    "code": "IT"
  }
}
```

Per una dissertazione eloquente sulla struttura dell’indirizzo
vedi ["FRANK'S COMPULSIVE GUIDE TO POSTAL ADDRESSES - Effective Addressing for International Mail"](http://www.columbia.edu/~fdc/postal/index.html#italy)


​4.15​ Canale di contatto per le notifiche - `contact`
----------------------------------------------------

Descrive il canale di contatto preferenziale indicato dall’acquirente (l’acquirente autonomamente potrà definirne più
d’uno tramite la interfaccia della applicazione Garanteasy) se diverso dalla mail usata per la login :

* `channel` - indirizzo di posta elettronica o numero telefonico (obbligatorio)
* `primaryContact` - valore booleano (1-vero/0-falso)che indica se quello specificato è da considerarsi il canale di
  contatto privilegiato dall’acquirente (obbligatorio)
* `contactType` - può assumere i valori `EMAIL` o `PHONE`
* `user` - struttura con i dati relativi alla login dell'utente

Per esempio:

```json
{
  "contact": {
    "channel": "[luca@garanteasy.com](mailto:luca@garanteasy.com)",
    "primaryContact": true,
    "user": {
      ...
    }
  }
}

```

4.16 Negozio - `shop`
---------------------

Negozio presso il quale è stato effettuato l’acquisto.

A ciascun partner viene comunicato l’elenco dei codici dei negozi per i quali comunica gli acquisti. Il codice deve
essere obbligatoriamente comunicato nella struttura "shop" così come il codice del partner di riferimento.

* `code` - stringa alfanumerica del codice del negozio
* `shopType` - può assumere i valori `PHYSICAL` o  `ECOMMERCE`
* `merchant` - struttura con il codice del partner concordato con Garanteasy

Per esempio:

```json
{
  "shop": {
    "code": "XXX",
    "merchant": {
      "code": "YYY"
    }
  }
}
```

​4.17​ Carte di fidelizzazione - `card`
-------------------------------------

Descrive le caratteristiche della eventuale carta di fidelizzazione utilizzata dall’acquirente al momento dell’acquisto:

* `number` - identificativo alfanumerico della carta (obbligatorio)
* `type` - tipologia di carta (obbligatorio)
* `activationDate` - data di attivazione della carta
* `exiperationDate` -data di disattivazione della carta

Le tipologie di carta sono precodificate (p.e. 'FIDATY' o 'CARTA-DIPENDENTE') e devono essere preventivamente
concordate, è possibile anche stabilire una validazione sul formato del numero di carta per ciascuna tipolgia (p.e. la
carta dipendente di XXX inizia con "999" ).

Per esempio:

```json
{
  "card": {
    "number": "FID0123456789",
    "cardType": {
      "code": "FIDATY"
    }
  }
}
```

4.18 Prodotti acquistati - `purchasedProducts`
--------------------------

L’elenco dei prodotti acquistati è una lista di strutture PurchasedProduct ciascuno con le seguenti caratteristiche:

* `serialNumber` - numero di serie identificativo della unità di prodotto acquistata (IMEI nel caso dei cellulari),
  verrà utilizzato nelle eventuali richieste di assistenza tecnica (se non fornito in questa fase verrà richiesto
  all’utente qualora questi dovesse richiedere un intervento in assistenza
* `deliveryDate` - data di consegna in formato `AAAA-MM-GG`
* `installationDate` - data di installazione in formato `AAAA-MM-GG`
* `quantity` - quantità di prodotti uguali acquistati
* `product` - struttura identificativa del prodotto (obbligatorio)
* `productName` - nome del prodotto come appare sul documento di acquisto
* `transactionAmount` - costo totale del/dei prodotti (prezzo unitario x quantità)
* `currency` - valuta
* `productCondition` - condizione del prodotto, può assumere i valori `NEW` (default) oppure `USED`

4.19 Codici dei prodotti
--------------------------

Se i codici dei prodotti acquistati corrispondono ai codici di prodotto esposti dal database ICECAT non è necessario
integrare le informazioni in alcun modo.

Viceverse se i prodotti acquistati NON dovessero essere presenti nel database ICECAT è necessario periodicamente
aggiornare il database prodotti di Garanteasy fornendo il catalogo prodotto come descritto nel documento "Allegato
Tecnico: "Servizi, Rating, Materiali, Software, Trasmissione Dati - Sintesi formato CSV".

NB i codici prodotto dei servizi Garanteasy sono elencati al
paragrafo "[Codici prodotto Garanteasy](codici-prodotto-garanteasy.md)".

4.20 Struttura `Product`
-----------------------

I prodotti sono caratterizzati da:

* `name` - nome del prodotto eventulmente differente dalla dicitura che appare sul documento di acquisto; è necessario
  assieme al brand per identificare il prodotto nel database prodotti qualora non fossero disponibili il codice ean o
  uno sku comunicato con il catalogo prodotti del partner
* `sku` - eventuale codice interno del partner (il catalogo del partner deve essere comunicato preventivamente)
* `eanCode` - ean che identifica il prodotto
* `transportable` - valoro booleano che indica se il prodotto è trasportabile o meno
* `brand` - codice del marchio del produttore
* `underWarranty` - booleano che indica se il prodotto è coperto da garanzia legale (per esempio sono esclusi da
  garanzia legale shoppers, profumi, saponi, cibo, ricariche telefoniche ...)