---
title: Garanteasy prepagato
---
10 Garanteasy prepagato
=========================

I documenti di acquisto possono essere comunicati con il prodotto avente codice `GARANTEASY-PREPAGATO`, il funzionamento delle associazioni delle garanzie etc non subisce alcuna variazione. I documenti con `GARANTEASY-PREPAGATO` vengono conteggiati a parte ai fini amministrativi.
