---
home: true
heroImage: /images/LOGO-payoff-colore-300dpi.jpg
heroText: API documentation
tagLine: tagline
description: Goto https://garanteasy.com for general info.
actionText: Get started →
actionLink: /intro/
features:
- title: Simplicity First
  details: Simple REST api, json structured data
- title: Secure
  details: JWT based authentication.
- title: Performant
  details: Respects cache directives.
footer: GARANTEASY S.R.L. Sede Legale in Piazza Pio XI, 1 - 20123 Milano - garanteasy@pec.it
---