---
title: Device o sito ecommerce di un merchant
---
​13.1​ Device o sito ecommerce di un merchant
---------------------------------------------

Quando il licenziatario è un merchant ha diritto (grazie alla informativa che fa sottoscrivere all’acquirente) a visualizzare le prove di acquisto relative agli acquisti effettuati presso i/il proprio esercizi/o; di seguito descriviamo il flusso delle chiamate e le impostazioni dei principali parametri delle strutture dati trasferite.

Questo flusso è adatto a device dedicati o standard (SmartPOS, App per dispositivi mobili, ...), web application  o ecommerce.

1.  Autenticazione POST:/authenticate con le proprie credenziali per reperire il token di autorizzazione da aggiungere all’header HTTP Authorization di tutte le chiamate successive.
2.  Reperimento dell’identificativo utente POST:/account
3.  Memorizzazione del documento di acquisto con parametri valorizzati come segue:

1.  source: MERCHANT
2.  shop: { code: CODICE dello SHOP }

TODO completare
