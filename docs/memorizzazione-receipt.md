---
title: Memorizzazione di una Receipt
---
​3​. API di Memorizzazione di una Receipt
========================================

Il sistema di memorizzazione delle Receipt può essere utilizzato dal licenziatario per caricare in modo sincrono le prove di acquisto e contestualmente valorizzare gli attributi del Customer che ha eseguito l'acquisto. La sequenza delle operazioni è la seguente:

``` mermaid
sequenceDiagram
Licenziatario->>Piattaforma: POST /api/authenticate(credentials)
Piattaforma-->>Licenziatario: id_token
Licenziatario->>Piattaforma: POST /api/receipts(id_token, receipt)
Piattaforma-->>Licenziatario: OK
```

La chiamata da eseguire è

    POST /receipt

i cui dati sono rappresentati in una struttura JSON come la seguente:

```json
{
  "receiptNumber": "STRING",
  "shopCode": "STRING",
  "buyingDate": "yyyyMMdd",
  "source": "MERCHANT",
  "cardNumber": "STRING",
  "cardType": "STRING",
  "validEmail": "STRING",
  "description": "TEXT",
  "customer": {
    "user": {
      "login": "STRING",
      "legalName": "TEXT",
      "address": {
        "street": "STRING",
        "postCode": "[0-9]+",
        "city": "STRING"
      }
    }
  },
  "purchasedProducts": [
    {
      "product": {
        "eanCode": "STRING"
      },
      "quantity": "NUMBER",
      "quantityUnit": "STRING"
    },
    ...
  ]
}
```

Per esempio:

    curl 'http://localhost:8081/api/receipts' -H 'Content-Type: application/json;charset=UTF-8' --data-binary  '{"receiptNumber":"111","description":"Documento memorizzato da HASCA","buyingDate":"2019-04-08","source":"MERCHANT","customer":{"legalName":"Luca Orlandi","user":{"login":"luca.orlandi@example.com"},"addresses":[{"street":"Via Trieste 63","locality":null,"city":"San Giuliano Milanese","region":null,"postcode":"20098","streetNumber":null,"country":{"code":"IT"}}]},"shop":{"code":"NEXI-HASCA-0211181617-N"}}'

La chiamata torna un [codice http](error-codes) che indica l'eventuale successo della chiamata. In particolare lo stato `409 Conflict` si presenta a seguito della richiesta di memorizzazione (POST) quando una ricevuta quando nel sistema è già presente un documento proveniente dal medesimo negozio, emesso nella medesima data con lo stesso numero di ricevuta e codice di cassa.

La risposta quando la memorizzazione va a buon fine è costituita da una struttura JSON che rappresenta il documento di acquisto; in particolare sono valorizzati gli identificativi delle entità memorizzate.

Per esempio:
```json
{
  "id":53422,
  "profile":null,
  "receiptNumber":"111",
  "description":"Documento memorizzato da HASCA",
  "creationDate":"2019-04-08",
  "buyingDate":"2019-04-08",
  "source":"MERCHANT",
  "warnings":[
    "customer",
    "contact",
    "shop.shopSignboard",
    "shop.merchant",
    "card",
    "ecommerce",
    "purchasedProducts",
    "receiptFiles",
    "counterCode",
    "paymentType",
    "receiptType",
    "transactionAmount",
    "currency"
  ],
  "shopId":2555,
  "unidentifiedUser":{
    "id":3697,
    "email":"luca.orlandi@example.com",
    "legalName":"Luca Orlandi",
    "address":{
      "id":13628,
      "street":"Via Trieste 63",
      "locality":null,
      "city":"San Giuliano Milanese",
      "region":null,
      "postcode":"20098",
      "streetNumber":null,
      "country":{
        "code":"IT",
        "name":"Italy"
      }
    }
  },
  "contact":null,
  "shop":{

  }
}
```
