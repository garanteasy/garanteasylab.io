---
title: Gestione degli errori
---
​11​ Gestione degli errori
==========================

Attraverso l’utilizzo delle API l’applicazione del licenziatario viene informata in tempo reale del successo dell'operazione di aggiornamento.

Vengono utilizzati i [comuni codici di errore del protocollo HTTP](https://restfulapi.net/http-status-codes/); in particolare:

*   200 - indica che l’operazione è andata a buon fine
*   201 - indica che l’entità inviata (ricevuta, file da allegare alla ricevuta) è stata creata
*   400 - significa che non sono state valorizzati tutti i campi obbligatori (il payload della risposta indica il dettaglio degli errori)
*   401 - viene presentato se non è stata eseguita correttamente la autorizzazione
*   403 - viene restituito se l’operazione non è autorizzata
*   404 - l’entità richiesta non è stata trovata
*   409 - indica che l'entità sottomessa va in conflitto con una gia esistente
*   ...
